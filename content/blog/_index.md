+++
title = "Blog"
template = "blog.html"
page_template = "blog-post.html"
sort_by = "date"
paginate_by = 5
paginate_path = "page"
+++